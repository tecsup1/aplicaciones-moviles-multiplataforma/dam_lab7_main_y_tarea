var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, TextInput, View, Button, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class TransferenceSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      importe: 0,
    };

  }
  render() {

    const {cuentaDeOrigen,cuentaDestino,importe,referencia,date,switchIsEnabled} = this.props.route.params;

    return (
      <View style={{width: '80%', alignSelf: 'center', marginVertical: 15}}>
        <Text>Cuenta origen</Text>
        <Text style={myStyles.textData}>{cuentaDeOrigen}</Text>

        <Text>Cuenta destino</Text>
        <Text style={myStyles.textData}>{cuentaDestino}</Text>

        <Text>Importe</Text>
        <Text style={myStyles.textData}>{importe}</Text>

        <Text>Referencia</Text>
        <Text style={myStyles.textData}>{referencia}</Text>

        <Text>Fecha</Text>
        <Text style={myStyles.textData}>{moment(date).format('DD MMM YYYY')}</Text>

        <Text>Mail</Text>
        <Text style={[myStyles.textData, {marginBottom: 15}]}>{switchIsEnabled?"SÍ":"NO"}</Text>

        <View
          style={{flexDirection: 'row', height: 50}}>
          {/*
           */}

          <View style={{flex: 2}}>
            <TouchableOpacity
              style={myStyles.backButton}
              onPress={() => this.props.navigation.navigate('First')}
              //style={[myStyles.backButton,{}]}
            >
              <Text style={{textAlign: 'center'}}>VOLVER</Text>
            </TouchableOpacity>
          </View>
          <View style={{flex:1}}></View>
          <View style={{flex: 2}}>
            <TouchableOpacity
              style={[myStyles.backButton,{backgroundColor:'#2b97c2'}]}
              onPress={() => this.props.navigation.navigate('Third')}
              //style={[myStyles.backButton,{}]}
            >
              <Text style={{textAlign: 'center'}}>CONFIRMAR</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const myStyles = StyleSheet.create({
  backButton: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: '#d1d1d1',
    justifyContent: 'center',
  },

  textData: {
    //height: 40,
    borderBottomWidth: 2,
    textAlignVertical: 'center',
    borderBottomColor: 'gray',
    marginBottom: 15,
  },
});
